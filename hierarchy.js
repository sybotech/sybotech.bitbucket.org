var hierarchy =
[
    [ "RNS::CameraInfo", "dc/d53/structRNS_1_1CameraInfo.html", null ],
    [ "RNS::Client", "d0/d07/classRNS_1_1Client.html", [
      [ "RNS::ChassisClient", "df/da7/classRNS_1_1ChassisClient.html", null ],
      [ "RNS::NavigatorClient", "de/d37/classRNS_1_1NavigatorClient.html", null ],
      [ "RNS::SensorClient", "d3/d25/classRNS_1_1SensorClient.html", null ],
      [ "RNS::SystemClient", "dd/d37/classRNS_1_1SystemClient.html", null ]
    ] ],
    [ "RNS::Connector", "d5/d8b/classRNS_1_1Connector.html", null ],
    [ "RNS::Joystick", "d4/dd1/structRNS_1_1Joystick.html", null ],
    [ "RNS::LaserInfo", "de/d52/structRNS_1_1LaserInfo.html", null ],
    [ "RNS::MapInfo", "d0/d44/structRNS_1_1MapInfo.html", null ],
    [ "RNS::Odometry", "dd/d5e/structRNS_1_1Odometry.html", null ],
    [ "RNS::RangerInfo", "d7/d70/structRNS_1_1RangerInfo.html", null ],
    [ "RNS::RobotDriverClient", "d3/d9f/classRNS_1_1RobotDriverClient.html", null ]
];