var classRNS_1_1ChassisClient =
[
    [ "MoveMode", "df/da7/classRNS_1_1ChassisClient.html#a170a712e4eb553162a2cb05039aac8e7", [
      [ "MoveDirect", "df/da7/classRNS_1_1ChassisClient.html#a170a712e4eb553162a2cb05039aac8e7af9241ca3f720487bbaa61abbd70fb94f", null ],
      [ "MoveOdom", "df/da7/classRNS_1_1ChassisClient.html#a170a712e4eb553162a2cb05039aac8e7a906ce288a655548fae54761f2def49ac", null ],
      [ "MoveNavigator", "df/da7/classRNS_1_1ChassisClient.html#a170a712e4eb553162a2cb05039aac8e7a751d66940569c2dd024b699742bcbaaf", null ]
    ] ],
    [ "TargetFrameType", "df/da7/classRNS_1_1ChassisClient.html#a456bcdeb7938860542b91cb0403e0c30", [
      [ "FrameGlobal", "df/da7/classRNS_1_1ChassisClient.html#a456bcdeb7938860542b91cb0403e0c30a547cb0a5c0e0976cfe3d24f376e90b10", null ],
      [ "FrameRobotCurrent", "df/da7/classRNS_1_1ChassisClient.html#a456bcdeb7938860542b91cb0403e0c30a4e525c16a5cdc9a79d4b0b434020560a", null ],
      [ "FrameLaser", "df/da7/classRNS_1_1ChassisClient.html#a456bcdeb7938860542b91cb0403e0c30ab5c08b1c01b1b46fdb4e56f9cac49879", null ]
    ] ],
    [ "ChassisClient", "df/da7/classRNS_1_1ChassisClient.html#a48d7351f714f52ea2d7bb0019241745d", null ],
    [ "setTargetFrame", "df/da7/classRNS_1_1ChassisClient.html#ae48587f0aa4837626eeae7421b10bee0", null ],
    [ "getTargetFrame", "df/da7/classRNS_1_1ChassisClient.html#aea0de14d636e2d22fb3c9aef14e7d04d", null ],
    [ "moveto", "df/da7/classRNS_1_1ChassisClient.html#a1e93b91d7c7c786817769ec74c0d157a", null ],
    [ "asyncMoveto", "df/da7/classRNS_1_1ChassisClient.html#af81f3b12fd3da3dfa00d5e1f79ab7225", null ],
    [ "drive", "df/da7/classRNS_1_1ChassisClient.html#a0e5e92ce5cd995153b0e4bb9ace4a42b", null ],
    [ "stop", "df/da7/classRNS_1_1ChassisClient.html#a946b305edcf682276a61d539b14af180", null ],
    [ "getVelocity", "df/da7/classRNS_1_1ChassisClient.html#acfa0cc469a9254fe54fe4a1a720c7444", null ],
    [ "getOdometry", "df/da7/classRNS_1_1ChassisClient.html#a4c12e232be122bb12f5e004dca3c02fa", null ],
    [ "impl", "df/da7/classRNS_1_1ChassisClient.html#aa42b3b4f1e90b22a5b9e6cb7fdfb2f83", null ]
];